package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/helper"
	"gitlab.com/market_system/ms_go_load_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type ArrivalRepo struct {
	db *pgxpool.Pool
}

func NewArrivalRepo(db *pgxpool.Pool) storage.ArrivalRepoI {
	return &ArrivalRepo{
		db: db,
	}
}

func (c *ArrivalRepo) Create(ctx context.Context, req *load_service.CreateArrival) (resp *load_service.ArrivalPrimaryKey, err error) {
	var (
		id          = uuid.New()
		lastCheckID string
	)

	queryID := `
			SELECT
				check_id
			FROM "arrival"
			ORDER BY created_at DESC
			LIMIT 1

	`
	err = c.db.QueryRow(ctx, queryID).Scan(&lastCheckID)

	prixodID := helper.GenerateID(lastCheckID, "P")

	query := `INSERT INTO "arrival" (
				id,
				branch_id,
				provider_id,
				check_id

			) VALUES ($1, $2, $3, $4)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.ProviderId,
		prixodID,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.ArrivalPrimaryKey{Id: id.String()}, nil

}

func (c *ArrivalRepo) GetByPKey(ctx context.Context, req *load_service.ArrivalPrimaryKey) (resp *load_service.Arrival, err error) {
	query := `
		SELECT
			id,
			branch_id,
			provider_id,
			status,
			check_id,    
			created_at,
			updated_at
		FROM "arrival"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branch_id   sql.NullString
		provider_id sql.NullString
		status      sql.NullString
		check_id    sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&provider_id,
		&status,
		&check_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &load_service.Arrival{
		Id:         id.String,
		BranchId:   branch_id.String,
		ProviderId: provider_id.String,
		Status:     status.String,
		CheckId:    check_id.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *ArrivalRepo) GetAll(ctx context.Context, req *load_service.GetListArrivalRequest) (resp *load_service.GetListArrivalResponse, err error) {

	resp = &load_service.GetListArrivalResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			provider_id,
			status,
			check_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "arrival"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branch_id   sql.NullString
			provider_id sql.NullString
			status      sql.NullString
			check_id    sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&provider_id,
			&status,
			&check_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Arrivals = append(resp.Arrivals, &load_service.Arrival{
			Id:         id.String,
			BranchId:   branch_id.String,
			ProviderId: provider_id.String,
			Status:     status.String,
			CheckId:    check_id.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *ArrivalRepo) Update(ctx context.Context, req *load_service.UpdateArrival) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "arrival"
			SET
				branch_id = :branch_id,
				provider_id = :provider_id,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"provider_id": req.GetProviderId(),
		"status":      req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *ArrivalRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"arrival"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *ArrivalRepo) Delete(ctx context.Context, req *load_service.ArrivalPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "arrival" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
