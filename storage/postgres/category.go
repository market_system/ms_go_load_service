package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/helper"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {
	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *load_service.CreateCategory) (resp *load_service.CategoryPrimaryKey, err error) {
	var (
		id = uuid.New()
	)
	query := `INSERT INTO "category" (
				id,
				name,
				parent_id,
				brand_id

			) VALUES ($1, $2, $3, $4)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.ParentId,
		req.BrandId,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.CategoryPrimaryKey{Id: id.String()}, nil

}

func (c *CategoryRepo) GetByPKey(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *load_service.Category, err error) {
	query := `
		SELECT
			id,
			name,
			parent_id,
			brand_id,  
			created_at,
			updated_at
		FROM "category"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		brand_id  sql.NullString
		parent_id sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&parent_id,
		&brand_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &load_service.Category{
		Id:        id.String,
		Name:      name.String,
		ParentId:  parent_id.String,
		BrandId:   brand_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *load_service.GetListCategoryRequest) (resp *load_service.GetListCategoryResponse, err error) {

	resp = &load_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			parent_id,
			brand_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "category"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			brand_id  sql.NullString
			parent_id sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&parent_id,
			&brand_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categorys = append(resp.Categorys, &load_service.Category{
			Id:        id.String,
			Name:      name.String,
			ParentId:  parent_id.String,
			BrandId:   brand_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *load_service.UpdateCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "category"
			SET
				brand_id = :brand_id,
				parent_id = :parent_id,
				name = :name,
				updated_at = now()
			WHERE
				id = :id
			`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"brand_id":  req.GetBrandId(),
		"parent_id": req.GetParentId(),
		"name":      req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *CategoryRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"category"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *CategoryRepo) Delete(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "category" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
