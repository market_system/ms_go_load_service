package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/helper"
	"gitlab.com/market_system/ms_go_load_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type BrandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) storage.BrandRepoI {
	return &BrandRepo{
		db: db,
	}
}

func (c *BrandRepo) Create(ctx context.Context, req *load_service.CreateBrand) (resp *load_service.BrandPrimaryKey, err error) {
	var (
		id = uuid.New()
	)
	query := `INSERT INTO "brand" (
				id,
				photo,
				name

			) VALUES ($1, $2, $3)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Photo,
		req.Name,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.BrandPrimaryKey{Id: id.String()}, nil

}

func (c *BrandRepo) GetByPKey(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *load_service.Brand, err error) {
	query := `
		SELECT
			id,
			photo,
			name,
			created_at,
			updated_at
		FROM "brand"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		photo     sql.NullString
		name      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &load_service.Brand{
		Id:        id.String,
		Photo:     photo.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *BrandRepo) GetAll(ctx context.Context, req *load_service.GetListBrandRequest) (resp *load_service.GetListBrandResponse, err error) {

	resp = &load_service.GetListBrandResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "brand"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			photo     sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Brands = append(resp.Brands, &load_service.Brand{
			Id:        id.String,
			Photo:     photo.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *BrandRepo) Update(ctx context.Context, req *load_service.UpdateBrand) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "brand"
			SET
				photo = :photo,
				name = :name,
				updated_at = now()
			WHERE
				id = :id
			`

	params = map[string]interface{}{
		"id":    req.GetId(),
		"photo": req.GetPhoto(),
		"name":  req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *BrandRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"brand"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *BrandRepo) Delete(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "brand" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
