package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/storage"
)

type Store struct {
	db              *pgxpool.Pool
	arrival         storage.ArrivalRepoI
	remainder       storage.RemainderRepoI
	product         storage.ProductRepoI
	category        storage.CategoryRepoI
	brand           storage.BrandRepoI
	provider        storage.ProviderRepoI
	arrival_product storage.ArrivalProductRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Arrival() storage.ArrivalRepoI {

	if s.arrival == nil {
		s.arrival = NewArrivalRepo(s.db)
	}

	return s.arrival
}

func (s *Store) Remainder() storage.RemainderRepoI {

	if s.remainder == nil {
		s.remainder = NewRemainderRepo(s.db)
	}

	return s.remainder
}

func (s *Store) Product() storage.ProductRepoI {

	if s.product == nil {
		s.product = NewProductRepo(s.db)
	}

	return s.product
}

func (s *Store) Category() storage.CategoryRepoI {

	if s.category == nil {
		s.category = NewCategoryRepo(s.db)
	}

	return s.category
}

func (s *Store) Brand() storage.BrandRepoI {

	if s.brand == nil {
		s.brand = NewBrandRepo(s.db)
	}

	return s.brand
}

func (s *Store) Provider() storage.ProviderRepoI {

	if s.provider == nil {
		s.provider = NewProviderRepo(s.db)
	}

	return s.provider
}

func (s *Store) ArrivalProduct() storage.ArrivalProductRepoI {

	if s.arrival_product == nil {
		s.arrival_product = NewArrivalProductRepo(s.db)
	}

	return s.arrival_product
}
