package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/helper"
	"gitlab.com/market_system/ms_go_load_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type ArrivalProductRepo struct {
	db *pgxpool.Pool
}

func NewArrivalProductRepo(db *pgxpool.Pool) storage.ArrivalProductRepoI {
	return &ArrivalProductRepo{
		db: db,
	}
}

func (c *ArrivalProductRepo) Create(ctx context.Context, req *load_service.CreateArrivalProduct) (resp *load_service.ArrivalProductPrimaryKey, err error) {
	var (
		id = uuid.New()
	)
	query := `INSERT INTO "arrival_product" (
				id,
				category_id,
				brand_id,
				product_id,
				bar_code,
				count,
				price,
				arrival_id

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.CategoryId,
		req.BrandId,
		req.ProductId,
		req.Barcode,
		req.Count,
		req.Price,
		req.ArrivalId,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.ArrivalProductPrimaryKey{Id: id.String()}, nil

}

func (c *ArrivalProductRepo) GetByPKey(ctx context.Context, req *load_service.ArrivalProductPrimaryKey) (resp *load_service.ArrivalProduct, err error) {
	query := `
		SELECT
			id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price, 
			arrival_id,
			created_at,
			updated_at
		FROM "arrival_product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		product_id  sql.NullString
		bar_code    sql.NullString
		count       int64
		price       int64
		arrival_id  sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_id,
		&brand_id,
		&product_id,
		&bar_code,
		&count,
		&price,
		&arrival_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &load_service.ArrivalProduct{
		Id:         id.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		ProductId:  product_id.String,
		Barcode:    bar_code.String,
		Count:      count,
		Price:      price,
		ArrivalId:  arrival_id.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *ArrivalProductRepo) GetAll(ctx context.Context, req *load_service.GetListArrivalProductRequest) (resp *load_service.GetListArrivalProductResponse, err error) {

	resp = &load_service.GetListArrivalProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price,
			arrival_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "arrival_product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			product_id  sql.NullString
			bar_code    sql.NullString
			count       int64
			price       int64
			arrival_id  sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&brand_id,
			&product_id,
			&bar_code,
			&count,
			&price,
			&arrival_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.ArrivalProducts = append(resp.ArrivalProducts, &load_service.ArrivalProduct{
			Id:         id.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			ProductId:  product_id.String,
			Barcode:    bar_code.String,
			Count:      count,
			Price:      price,
			ArrivalId:  arrival_id.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *ArrivalProductRepo) Update(ctx context.Context, req *load_service.UpdateArrivalProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "arrival_product"
			SET
				category_id = :category_id,
				brand_id = :brand_id,
				product_id = :product_id,
				bar_code = :bar_code,
				count = :count,
				price = :price,
				arrival_id = :arrival_id,
				updated_at = now()
			WHERE
				id = :id
			`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"product_id":  req.GetProductId(),
		"bar_code":    req.GetBarcode(),
		"count":       req.GetCount(),
		"price":       req.GetPrice(),
		"arrival_id":  req.GetArrivalId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *ArrivalProductRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"arrival_product"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *ArrivalProductRepo) Delete(ctx context.Context, req *load_service.ArrivalProductPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "arrival_product" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
