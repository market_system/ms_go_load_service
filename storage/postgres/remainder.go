package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/helper"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type RemainderRepo struct {
	db *pgxpool.Pool
}

func NewRemainderRepo(db *pgxpool.Pool) storage.RemainderRepoI {
	return &RemainderRepo{
		db: db,
	}
}

func (c *RemainderRepo) Create(ctx context.Context, req *load_service.CreateRemainder) (resp *load_service.RemainderPrimaryKey, err error) {
	var (
		id           = uuid.New()
		productPrice int64
	)
	query := `INSERT INTO "remainder" (
				id,
				branch_id,
				category_id,
				brand_id,
				product_id,
				name,
				bar_code,
				count,
				price

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`
	rowQuery := `
		SELECT
			p.price as price
		FROM "product" as p
		JOIN "remainder" AS r ON p.remainder_id = r.id
	`
	err = c.db.QueryRow(ctx, rowQuery).Scan(&productPrice)
	price := req.GetCount() * productPrice
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.CategoryId,
		req.BrandId,
		req.ProductId,
		req.Name,
		req.Barcode,
		req.Count,
		price,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.RemainderPrimaryKey{Id: id.String()}, nil

}

func (c *RemainderRepo) GetByPKey(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *load_service.Remainder, err error) {
	query := `
		SELECT
			id,
			branch_id,
			category_id,
			brand_id,
			product_id,
			name,
			bar_code,
			count,
			price,  
			created_at,
			updated_at
		FROM "remainder"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branch_id   sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		product_id  sql.NullString
		name        sql.NullString
		bar_code    sql.NullString
		count       int64
		price       int64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&category_id,
		&brand_id,
		&product_id,
		&name,
		&bar_code,
		&count,
		&price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &load_service.Remainder{
		Id:         id.String,
		BranchId:   branch_id.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		ProductId:  product_id.String,
		Name:       name.String,
		Barcode:    bar_code.String,
		Count:      count,
		Price:      price,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *RemainderRepo) GetAll(ctx context.Context, req *load_service.GetListRemainderRequest) (resp *load_service.GetListRemainderResponse, err error) {

	resp = &load_service.GetListRemainderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			category_id,
			brand_id,
			product_id,
			name,
			bar_code,
			count,
			price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "remainder"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branch_id   sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			product_id  sql.NullString
			name        sql.NullString
			bar_code    sql.NullString
			count       int64
			price       int64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&category_id,
			&brand_id,
			&product_id,
			&name,
			&bar_code,
			&count,
			&price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Remainders = append(resp.Remainders, &load_service.Remainder{
			Id:         id.String,
			BranchId:   branch_id.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			ProductId:  product_id.String,
			Name:       name.String,
			Barcode:    bar_code.String,
			Count:      count,
			Price:      price,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *RemainderRepo) Update(ctx context.Context, req *load_service.UpdateRemainder) (rowsAffected int64, err error) {

	var (
		query        string
		params       map[string]interface{}
		productPrice int
	)

	query = `
			UPDATE
			    "remainder"
			SET
				branch_id = :branch_id,
				category_id = :category_id,
				brand_id = :brand_id,
				product_id = :product_id,
				name = :name,
				bar_code = :bar_code,
				count = :count,
				price = :price,
				updated_at = now()
			WHERE
				id = :id
			`
	queryRow := `
		SELECT 
			p.price as price
		FROM "product" AS p
		JOIN "remainder" AS r ON r.product_id = p.id
	`
	err = c.db.QueryRow(ctx, queryRow).Scan(&productPrice)

	price := (req.GetCount() * int64(productPrice))

	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"product_id":  req.GetProductId(),
		"name":        req.GetName(),
		"bar_code":    req.GetBarcode(),
		"count":       req.GetCount(),
		"price":       price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *RemainderRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"remainder"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *RemainderRepo) Delete(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "remainder" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
