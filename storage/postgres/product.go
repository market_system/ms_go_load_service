package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/helper"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *load_service.CreateProduct) (resp *load_service.ProductPrimaryKey, err error) {
	var (
		id = uuid.New()
	)
	query := `INSERT INTO "product" (
				id,
				photo,
				name,
				category_id,
				brand_id,
				bar_code,
				price,
				count,
				remainder_id

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Photo,
		req.Name,
		req.CategoryId,
		req.BrandId,
		req.Barcode,
		req.Price,
		req.Count,
		req.RemainderId,
	)
	if err != nil {
		return nil, err
	}

	return &load_service.ProductPrimaryKey{Id: id.String()}, nil

}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error) {
	query := `
		SELECT
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price,  
			count,
			remainder_id,
			created_at,
			updated_at
		FROM "product"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		photo        sql.NullString
		name         sql.NullString
		brand_id     sql.NullString
		category_id  sql.NullString
		bar_code     sql.NullString
		price        int64
		count        int64
		remainder_id sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&category_id,
		&brand_id,
		&bar_code,
		&price,
		&count,
		&remainder_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return 
	}

	resp = &load_service.Product{
		Id:          id.String,
		Photo:       photo.String,
		CategoryId:  category_id.String,
		BrandId:     brand_id.String,
		Name:        name.String,
		Barcode:     bar_code.String,
		Price:       price,
		Count:       count,
		RemainderId: remainder_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *load_service.GetListProductRequest) (resp *load_service.GetListProductResponse, err error) {

	resp = &load_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price,
			count,
			remainder_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			photo        sql.NullString
			name         sql.NullString
			brand_id     sql.NullString
			category_id  sql.NullString
			bar_code     sql.NullString
			price        int64
			count        int64
			remainder_id sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&category_id,
			&brand_id,
			&bar_code,
			&price,
			&count,
			&remainder_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &load_service.Product{
			Id:          id.String,
			Photo:       photo.String,
			CategoryId:  category_id.String,
			BrandId:     brand_id.String,
			Name:        name.String,
			Barcode:     bar_code.String,
			Price:       price,
			Count:       count,
			RemainderId: remainder_id.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *ProductRepo) Update(ctx context.Context, req *load_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "product"
			SET
				brand_id = :brand_id,
				category_id = :category_id,
				photo = :photo,
				name = :name,
				bar_code = :bar_code,
				price = :price,
				remainder_id = :remainder_id,
				count = :count,
				updated_at = now()
			WHERE
				id = :id
			`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"brand_id":     req.GetBrandId(),
		"category_id":  req.GetCategoryId(),
		"photo":        req.GetPhoto(),
		"name":         req.GetName(),
		"bar_code":     req.GetBarcode(),
		"price":        req.GetPrice(),
		"remainder_id": req.GetRemainderId(),
		"count":        req.GetCount(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *ProductRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"product"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *ProductRepo) Delete(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "product" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
