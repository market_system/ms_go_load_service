package storage

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/models"
	"google.golang.org/protobuf/types/known/emptypb"
)

type StorageI interface {
	CloseDB()
	Arrival() ArrivalRepoI
	Remainder() RemainderRepoI
	Product() ProductRepoI
	Category() CategoryRepoI
	Brand() BrandRepoI
	Provider() ProviderRepoI
	ArrivalProduct() ArrivalProductRepoI
}
type ArrivalRepoI interface {
	Create(ctx context.Context, req *load_service.CreateArrival) (resp *load_service.ArrivalPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.ArrivalPrimaryKey) (resp *load_service.Arrival, err error)
	GetAll(ctx context.Context, req *load_service.GetListArrivalRequest) (resp *load_service.GetListArrivalResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateArrival) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ArrivalPrimaryKey) (*emptypb.Empty, error)
}

type RemainderRepoI interface {
	Create(ctx context.Context, req *load_service.CreateRemainder) (resp *load_service.RemainderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *load_service.Remainder, err error)
	GetAll(ctx context.Context, req *load_service.GetListRemainderRequest) (resp *load_service.GetListRemainderResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateRemainder) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.RemainderPrimaryKey) (*emptypb.Empty, error)
}
type ProductRepoI interface {
	Create(ctx context.Context, req *load_service.CreateProduct) (resp *load_service.ProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error)
	GetAll(ctx context.Context, req *load_service.GetListProductRequest) (resp *load_service.GetListProductResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateProduct) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ProductPrimaryKey) (*emptypb.Empty, error)
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *load_service.CreateCategory) (resp *load_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *load_service.Category, err error)
	GetAll(ctx context.Context, req *load_service.GetListCategoryRequest) (resp *load_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateCategory) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.CategoryPrimaryKey) (*emptypb.Empty, error)
}

type BrandRepoI interface {
	Create(ctx context.Context, req *load_service.CreateBrand) (resp *load_service.BrandPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *load_service.Brand, err error)
	GetAll(ctx context.Context, req *load_service.GetListBrandRequest) (resp *load_service.GetListBrandResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateBrand) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.BrandPrimaryKey) (*emptypb.Empty, error)
}

type ProviderRepoI interface {
	Create(ctx context.Context, req *load_service.CreateProvider) (resp *load_service.ProviderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.ProviderPrimaryKey) (resp *load_service.Provider, err error)
	GetAll(ctx context.Context, req *load_service.GetListProviderRequest) (resp *load_service.GetListProviderResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateProvider) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ProviderPrimaryKey) (*emptypb.Empty, error)
}

type ArrivalProductRepoI interface {
	Create(ctx context.Context, req *load_service.CreateArrivalProduct) (resp *load_service.ArrivalProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *load_service.ArrivalProductPrimaryKey) (resp *load_service.ArrivalProduct, err error)
	GetAll(ctx context.Context, req *load_service.GetListArrivalProductRequest) (resp *load_service.GetListArrivalProductResponse, err error)
	Update(ctx context.Context, req *load_service.UpdateArrivalProduct) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *load_service.ArrivalProductPrimaryKey) (*emptypb.Empty, error)
}
