package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ArrivalProductService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedArrivalProductServiceServer
}

func NewArrivalProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ArrivalProductService {
	return &ArrivalProductService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *ArrivalProductService) Create(ctx context.Context, req *load_service.CreateArrivalProduct) (resp *load_service.ArrivalProduct, err error) {

	u.log.Info("-----------CreateArrivalProduct----------", logger.Any("req", req))

	id, err := u.strg.ArrivalProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateArrivalProduct ->ArrivalProduct->Create", logger.Error(err))
		return
	}

	prod, err := u.strg.Product().GetByPKey(ctx, &load_service.ProductPrimaryKey{Id: req.ProductId})
	if err != nil {
		u.log.Error("CreateArrivalProduct ->Product->GETBYPKEY", logger.Error(err))
		return
	}

	rem, err := u.strg.Remainder().GetByPKey(ctx, &load_service.RemainderPrimaryKey{Id: prod.RemainderId})
	if err != nil {
		u.log.Error("CreateArrivalProduct ->Remainder->GETBYPKEY", logger.Error(err))
		return
	}

	_, err = u.strg.Product().Update(ctx, &load_service.UpdateProduct{
		Id:          prod.Id,
		Photo:       prod.Photo,
		Name:        prod.Name,
		CategoryId:  req.CategoryId,
		BrandId:     req.BrandId,
		Barcode:     req.Barcode,
		Price:       req.Price,
		RemainderId: prod.RemainderId,
		Count:       prod.Count + req.Count,
	})
	if err != nil {
		u.log.Error("CreateArrivalProduct ->Product->UPDATE", logger.Error(err))
		return
	}

	_, err = u.strg.Remainder().Update(ctx, &load_service.UpdateRemainder{
		Id:         rem.Id,
		BranchId:   rem.BranchId,
		CategoryId: rem.CategoryId,
		ProductId:  rem.ProductId,
		BrandId:    rem.BrandId,
		Name:       rem.Name,
		Count:      rem.Count + req.Count,
		Barcode:    rem.Barcode,
	})
	if err != nil {
		u.log.Error("CreateArrivalProduct ->Remainder->UPDATE", logger.Error(err))
		return
	}

	resp, err = u.strg.ArrivalProduct().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdArrivalProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ArrivalProductService) GetByID(ctx context.Context, req *load_service.ArrivalProductPrimaryKey) (resp *load_service.ArrivalProduct, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.ArrivalProduct().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->ArrivalProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ArrivalProductService) GetList(ctx context.Context, req *load_service.GetListArrivalProductRequest) (*load_service.GetListArrivalProductResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.ArrivalProduct().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->ArrivalProduct->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *ArrivalProductService) Update(ctx context.Context, req *load_service.UpdateArrivalProduct) (resp *load_service.ArrivalProduct, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.ArrivalProduct().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->ArrivalProduct->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.ArrivalProduct().GetByPKey(ctx, &load_service.ArrivalProductPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->ArrivalProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *ArrivalProductService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchArrivalProduct) (resp *load_service.ArrivalProduct, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.ArrivalProduct().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchArrivalProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ArrivalProduct().GetByPKey(ctx, &load_service.ArrivalProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->ArrivalProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *ArrivalProductService) Delete(ctx context.Context, req *load_service.ArrivalProductPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.ArrivalProduct().Delete(ctx, &load_service.ArrivalProductPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->ArrivalProduct->Delete", logger.Error(err))
		return
	}

	return
}
