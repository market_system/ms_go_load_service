package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ArrivalService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedArrivalServiceServer
}

func NewArrivalService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ArrivalService {
	return &ArrivalService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *ArrivalService) Create(ctx context.Context, req *load_service.CreateArrival) (resp *load_service.Arrival, err error) {

	u.log.Info("-----------CreateArrival----------", logger.Any("req", req))

	id, err := u.strg.Arrival().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateArrival ->Arrival->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Arrival().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdArrival->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ArrivalService) GetByID(ctx context.Context, req *load_service.ArrivalPrimaryKey) (resp *load_service.Arrival, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Arrival().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Arrival->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ArrivalService) GetList(ctx context.Context, req *load_service.GetListArrivalRequest) (*load_service.GetListArrivalResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Arrival().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Arrival->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *ArrivalService) Update(ctx context.Context, req *load_service.UpdateArrival) (resp *load_service.Arrival, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Arrival().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Arrival->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Arrival().GetByPKey(ctx, &load_service.ArrivalPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Arrival->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *ArrivalService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchArrival) (resp *load_service.Arrival, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Arrival().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchArrival--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Arrival().GetByPKey(ctx, &load_service.ArrivalPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Arrival->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *ArrivalService) Delete(ctx context.Context, req *load_service.ArrivalPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Arrival().Delete(ctx, &load_service.ArrivalPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Arrival->Delete", logger.Error(err))
		return
	}

	return
}
