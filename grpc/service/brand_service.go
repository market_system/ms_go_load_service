package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BrandService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedBrandServiceServer
}

func NewBrandService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BrandService {
	return &BrandService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *BrandService) Create(ctx context.Context, req *load_service.CreateBrand) (resp *load_service.Brand, err error) {

	u.log.Info("-----------CreateBrand----------", logger.Any("req", req))

	id, err := u.strg.Brand().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateBrand ->Brand->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Brand().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdBrand->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *BrandService) GetByID(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *load_service.Brand, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Brand().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Brand->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *BrandService) GetList(ctx context.Context, req *load_service.GetListBrandRequest) (*load_service.GetListBrandResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Brand().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Brand->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *BrandService) Update(ctx context.Context, req *load_service.UpdateBrand) (resp *load_service.Brand, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Brand().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Brand->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Brand().GetByPKey(ctx, &load_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Brand->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *BrandService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchBrand) (resp *load_service.Brand, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Brand().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchBrand--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Brand().GetByPKey(ctx, &load_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *BrandService) Delete(ctx context.Context, req *load_service.BrandPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Brand().Delete(ctx, &load_service.BrandPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Brand->Delete", logger.Error(err))
		return
	}

	return
}
