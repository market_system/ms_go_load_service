package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type CategoryService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *CategoryService {
	return &CategoryService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *CategoryService) Create(ctx context.Context, req *load_service.CreateCategory) (resp *load_service.Category, err error) {

	u.log.Info("-----------CreateCategory----------", logger.Any("req", req))

	id, err := u.strg.Category().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateCategory ->Category->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Category().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdCategory->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *CategoryService) GetByID(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *load_service.Category, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Category().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Category->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *CategoryService) GetList(ctx context.Context, req *load_service.GetListCategoryRequest) (*load_service.GetListCategoryResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Category().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Category->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *CategoryService) Update(ctx context.Context, req *load_service.UpdateCategory) (resp *load_service.Category, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Category().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Category->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Category().GetByPKey(ctx, &load_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Category->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *CategoryService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchCategory) (resp *load_service.Category, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Category().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByPKey(ctx, &load_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *CategoryService) Delete(ctx context.Context, req *load_service.CategoryPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Category().Delete(ctx, &load_service.CategoryPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Category->Delete", logger.Error(err))
		return
	}

	return
}
