package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ProviderService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedProviderServiceServer
}

func NewProviderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ProviderService {
	return &ProviderService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *ProviderService) Create(ctx context.Context, req *load_service.CreateProvider) (resp *load_service.Provider, err error) {

	u.log.Info("-----------CreateProvider----------", logger.Any("req", req))

	id, err := u.strg.Provider().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateProvider ->Provider->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Provider().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdProvider->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ProviderService) GetByID(ctx context.Context, req *load_service.ProviderPrimaryKey) (resp *load_service.Provider, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Provider().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Provider->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ProviderService) GetList(ctx context.Context, req *load_service.GetListProviderRequest) (*load_service.GetListProviderResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Provider().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Provider->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *ProviderService) Update(ctx context.Context, req *load_service.UpdateProvider) (resp *load_service.Provider, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Provider().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Provider->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Provider().GetByPKey(ctx, &load_service.ProviderPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Provider->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *ProviderService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchProvider) (resp *load_service.Provider, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Provider().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchProvider--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Provider().GetByPKey(ctx, &load_service.ProviderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Provider->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *ProviderService) Delete(ctx context.Context, req *load_service.ProviderPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Provider().Delete(ctx, &load_service.ProviderPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Provider->Delete", logger.Error(err))
		return
	}

	return
}
