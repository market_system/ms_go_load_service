package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ProductService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *ProductService) Create(ctx context.Context, req *load_service.CreateProduct) (resp *load_service.Product, err error) {

	u.log.Info("-----------CreateProduct----------", logger.Any("req", req))

	id, err := u.strg.Product().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateProduct ->Product->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Product().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ProductService) GetByID(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *load_service.Product, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Product().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Product->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ProductService) GetList(ctx context.Context, req *load_service.GetListProductRequest) (*load_service.GetListProductResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Product().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Product->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *ProductService) Update(ctx context.Context, req *load_service.UpdateProduct) (resp *load_service.Product, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Product().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Product->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Product().GetByPKey(ctx, &load_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Product->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *ProductService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchProduct) (resp *load_service.Product, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Product().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &load_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *ProductService) Delete(ctx context.Context, req *load_service.ProductPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Product().Delete(ctx, &load_service.ProductPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Product->Delete", logger.Error(err))
		return
	}

	return
}
