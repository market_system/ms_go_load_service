package service

import (
	"context"

	"gitlab.com/market_system/ms_go_load_service/config"
	"gitlab.com/market_system/ms_go_load_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_load_service/grpc/client"
	"gitlab.com/market_system/ms_go_load_service/models"
	"gitlab.com/market_system/ms_go_load_service/pkg/logger"
	"gitlab.com/market_system/ms_go_load_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type RemainderService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*load_service.UnimplementedRemainderServiceServer
}

func NewRemainderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *RemainderService {
	return &RemainderService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *RemainderService) Create(ctx context.Context, req *load_service.CreateRemainder) (resp *load_service.Remainder, err error) {

	u.log.Info("-----------CreateRemainder----------", logger.Any("req", req))

	id, err := u.strg.Remainder().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateRemainder ->Remainder->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Remainder().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdRemainder->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *RemainderService) GetByID(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *load_service.Remainder, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Remainder().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Remainder->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *RemainderService) GetList(ctx context.Context, req *load_service.GetListRemainderRequest) (*load_service.GetListRemainderResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Remainder().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Remainder->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *RemainderService) Update(ctx context.Context, req *load_service.UpdateRemainder) (resp *load_service.Remainder, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Remainder().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Remainder->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Remainder().GetByPKey(ctx, &load_service.RemainderPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Remainder->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *RemainderService) UpdatePatch(ctx context.Context, req *load_service.UpdatePatchRemainder) (resp *load_service.Remainder, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Remainder().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchRemainder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Remainder().GetByPKey(ctx, &load_service.RemainderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Remainder->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *RemainderService) Delete(ctx context.Context, req *load_service.RemainderPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Remainder().Delete(ctx, &load_service.RemainderPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Remainder->Delete", logger.Error(err))
		return
	}

	return
}
