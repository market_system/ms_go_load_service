package client

import "gitlab.com/market_system/ms_go_load_service/config"

type ServiceManagerI interface {
}

type grpcClients struct {
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// connLoadService, err := grpc.Dial(
	// 	cfg.LOADServiceHost+cfg.LOADGRPCPort,
	// 	grpc.WithTransportCredentials(insecure.NewCredentials()),
	// )
	// if err != nil {
	// 	return nil, err
	// }

	return &grpcClients{}, nil
}
