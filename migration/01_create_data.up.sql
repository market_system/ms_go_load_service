
CREATE TABLE "brand" (
  "id" UUID PRIMARY KEY NOT NULL,
  "photo" varchar,
  "name" varchar(45),
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "category" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar(45),
  "parent_id" UUID,
  "brand_id" UUID,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "product" (
  "id" UUID PRIMARY KEY NOT NULL,
  "photo" varchar,
  "name" varchar(45),
  "category_id" UUID,
  "brand_id" UUID,
  "bar_code" varchar,
  "price" NUMERIC,
  "count" NUMERIC,
  "remainder_id" UUID,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "provider" (
  "id" UUID PRIMARY KEY NOT NULL,
  "name" varchar,
  "phone_number" varchar,
  "active" boolean DEFAULT true,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


CREATE TABLE "arrival_product" (
  "id" UUID PRIMARY KEY NOT NULL,
  "category_id" UUID,
  "brand_id" UUID,
  "bar_code" varchar,
  "product_id" UUID,
  "arrival_id" UUID,
  "count" int,
  "price" NUMERIC,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "arrival" (
  "id" UUID PRIMARY KEY NOT NULL,
  "branch_id" UUID,
  "provider_id" UUID,
  "status" varchar DEFAULT 'in_proccess',
  "check_id" varchar,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "remainder" (
  "id" UUID PRIMARY KEY NOT NULL,
  "branch_id" UUID,
  "category_id" UUID,
  "brand_id" UUID,
  "product_id" UUID,
  "name" varchar,
  "bar_code" varchar,
  "count" int,
  "price" NUMERIC,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);




ALTER TABLE "category" ADD FOREIGN KEY ("brand_id") REFERENCES "brand" ("id");

ALTER TABLE "product" ADD FOREIGN KEY ("category_id") REFERENCES "category" ("id");

ALTER TABLE "product" ADD FOREIGN KEY ("brand_id") REFERENCES "brand" ("id");

ALTER TABLE "product" ADD FOREIGN KEY ("remainder_id") REFERENCES "remainder" ("id");

ALTER TABLE "arrival_product" ADD FOREIGN KEY ("product_id") REFERENCES "product" ("id");

ALTER TABLE "arrival_product" ADD FOREIGN KEY ("category_id") REFERENCES "category" ("id");

ALTER TABLE "arrival_product" ADD FOREIGN KEY ("brand_id") REFERENCES "brand" ("id");

ALTER TABLE "arrival" ADD FOREIGN KEY ("provider_id") REFERENCES "provider" ("id");

ALTER TABLE "remainder" ADD FOREIGN KEY ("category_id") REFERENCES "category" ("id");

ALTER TABLE "remainder" ADD FOREIGN KEY ("brand_id") REFERENCES "brand" ("id");
